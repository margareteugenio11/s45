npx create-react-app course-booking

For Clean Slate:
1. Remove the following files
	App.test.js
	index.css
	logo.svg
	reportWebVitals.js

2. Remove the importations in index.js such as the css and reportWebVitals

3. Remove the importation of logo in App.js and the return div

JSX (JavaScript XML)
>> HTML - like codes
>> the difference between the HTML codes is that you can insert a Javascript logic in it.

<HTML>

<button disabled> </button>

>JSX<

<button if(){
	
} else {
	
}> </button>

React
>Mounting
	>displays the component
>Rendering
	>process of invoking a component

Organizing import modules
	1. Import built-in react modules
		>>like React and Fragment
	2. import installed/downloaded packages
		>>react-bootstrap, swal
	3. import from user defined components
		>> components and pages

React follows Pascal Case in terms of naming conventions
	>> capitalized letters for all words of the function AND the file name associated with it.

React is a SPA
	>> Single Page Application
