import {Row, Col, Card, Button} from 'react-bootstrap'

export default function Banner(){
	return (
		<Row>
			<Col>
				<Card className ="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h3>Sample Course</h3>
						</Card.Title>
						
						<Card.Text>
							<div>
								Description<br/>
								This is a sample course offering<br/>
							</div>

							<div className="my-3">
								Price:<br/>
								Php 43,000 <br/>
							</div>

							<Button variant = "primary">Enroll</Button>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
